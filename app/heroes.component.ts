import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hero } from './hero';

import { HeroService } from './hero.service';

// decorator that associates metadata with he AppComponent class

// A component class that controls the appearance and behavior 
// of a view through its template

@Component({
    moduleId: module.id,
    selector: 'my-heroes',   //  that specifies a simple CSS selector 
    // for an HTML element that represent teh component
    // template that tells angular how to render
    // the component view
    templateUrl: 'template/heroes.component.html',
    styleUrls: ['css/heroes.component.css']  // arrays
})


export class HeroesComponent implements OnInit {
    title = "Tour of herous";
    hero: Hero = {
        id: 1,
        name: 'WindStorm'
    };
    heroes: Hero[];
    selectedHero: Hero;
    onSelect(hero: Hero): void {
        this.selectedHero = hero;
    }
    constructor(
        private heroService: HeroService,
        private router: Router
    ) { }
    getHeroes(): void {
        this.heroService.getHeroes().then(heroes =>
            this.heroes = heroes);
    }

    ngOnInit(): void {
        this.getHeroes();
    }
    gotoDetail(): void {
        this.router.navigate(['/detail',
            this.selectedHero.id]);
    }

    add(name: string): void {
        name = name.trim();
        if (!name) { return; }
        this.heroService.create(name)
            .then(hero => {
                this.heroes.push(hero);
                this.selectedHero = null;
            })
    }

    delete(hero: Hero): void {
        this.heroService
            .delete(hero.id)
            .then(() => {
                this.heroes = this.heroes.filter(h => h !== hero);
                if (this.selectedHero === hero) {
                    this.selectedHero = null
                }
            })
    }
}




